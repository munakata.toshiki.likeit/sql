select t1.category_name,
sum(t.item_price) as total_price
from 
item t
inner join item_category t1
on t.category_id=t1.category_id 

group BY
t.category_id

ORDER BY
    total_price desc;
